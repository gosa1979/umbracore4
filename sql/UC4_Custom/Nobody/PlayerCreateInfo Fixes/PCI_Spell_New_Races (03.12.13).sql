
-- Nagas 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1, 668, "Language Common"); 		 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 2, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 4, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 8, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 16, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 32, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 64, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 128, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 256, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1024, 668, "Language Common");

-- Broken
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1, 668, "Language Common"); 		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 2, 668, "Language Common"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 4, 668, "Language Common"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 8, 668, "Language Common"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 16, 668, "Language Common"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 32, 668, "Language Common"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 64, 668, "Language Common"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 128, 668, "Language Common"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 256, 668, "Language Common"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1024, 668, "Language Common"); 

-- Horde Talk

-- Vrykul
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1, 669, "Language Orcish");		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 2, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 4, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 8, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 16, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 32, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 64, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 128, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 256, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1024, 669, "Language Orcish");

-- Goblin
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 2, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 4, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 8, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 16, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 32, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 64, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 128, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 256, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1024, 669, "Language Orcish");

-- Heroic Strike (78) Naga, Broken, Vrykul, Goblin
-- Warrior
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x1, 78, "Heroic Strike");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x1, 78, "Heroic Strike");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x1, 78, "Heroic Strike");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x1, 78, "Heroic Strike");

-- Dodge(81)  Naga, Broken, Vrykul, Goblin
-- Warrior
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x1, 81, "Dodge");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x1, 81, "Dodge");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x1, 81, "Dodge");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x1, 81, "Dodge");

-- Block (107) Naga, Broken, Vrykul, Goblin
-- Warrior
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x1, 107, "Block");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x1, 107, "Block");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x1, 107, "Block");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x1, 107, "Block");

-- One-Handed Axes (196) Naga, Broken, Vrykul, Goblin
-- Warrior
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x1, 196, "One-Handed Axes");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x1, 196, "One-Handed Axes");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x1, 196, "One-Handed Axes");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x1, 196, "One-Handed Axes");

-- One-Handed Maces (198) Naga, Broken, Vrykul, Goblin
--Warrior
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x1, 198, "One-Handed Maces");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x1, 198, "One-Handed Maces");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x1, 198, "One-Handed Maces");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x1, 198, "One-Handed Maces");

-- One-Handed Swords (201) Naga, Broken, Vrykul, Goblin
-- Warrior
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x1, 201, "One-Handed Swords");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x1, 201, "One-Handed Swords");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x1, 201, "One-Handed Swords");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x1, 201, "One-Handed Swords");

-- Unarmed (203) Naga, Broken, Vrykul, Goblin
-- All
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 203, "Unarmed");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 203, "Unarmed");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 203, "Unarmed");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 203, "Unarmed");

-- Defense (204) Naga, Broken, Vrykul, Goblin
-- All
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 204, "Defense");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 204, "Defense");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 204, "Defense");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 204, "Defense");

-- SPELLDEFENSE (DND) [522] Naga, Broken, Vrykul, Goblin
-- All
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 204, "SPELLDEFENSE DND");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 204, "SPELLDEFENSE DND");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 204, "SPELLDEFENSE DND");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 204, "SPELLDEFENSE DND");

-- Language Common (668) Naga, Broken, Vrykul, Goblin
-- All
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 668, "Language Common");

-- Disarm (1843) Naga, Broken, Vrykul, Goblin
-- All
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 1843, "Disarm");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 1843, "Disarm");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 1843, "Disarm");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 1843, "Disarm");

-- Generic (2382) Naga, Broken, Vrykul, Goblin
-- All
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 2382, "Generic");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 2382, "Generic");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 2382, "Generic");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 2382, "Generic");

-- Battle Stance (2457) Naga, Broken, Vrykul, Goblin
-- Warrior
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x1, 2457, "Battle Stance");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x1, 2457, "Battle Stance");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x1, 2457, "Battle Stance");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x1, 2457, "Battle Stance");

-- Honorless Target (2479) Naga, Broken, Vrykul, Goblin
-- ALL 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 2479, "Honorless Target");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 2479, "Honorless Target");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 2479, "Honorless Target");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 2479, "Honorless Target");

-- Detect (3050) Naga, Broken, Vrykul, Goblin
-- ALL
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 3050, "Detect");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 3050, "Detect");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 3050, "Detect");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 3050, "Detect");

-- Opening (3365) Naga, Broken, Vrykul, Goblin
-- ALL
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 3365, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 3365, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 3365, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 3365, "Opening");

-- Defensive State (DND) [5301] Naga, Broken, Vrykul, Goblin
-- Warrior
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x1, 5301, "Defensive State");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x1, 5301, "Defensive State");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x1, 5301, "Defensive State");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x1, 5301, "Defensive State");

-- Closing (6233) Naga, Broken, Vrykul, Goblin
-- All
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 6233, "Closing");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 6233, "Closing");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 6233, "Closing");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 6233, "Closing");

-- Closing (6246) Naga, Broken, Vrykul, Goblin
-- ALL 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 6246, "Closing");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 6246, "Closing");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 6246, "Closing");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 6246, "Closing");

-- Opening (6247) Naga, Broken, Vrykul, Goblin
-- ALL 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 6247, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 6247, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 6247, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 6247, "Opening");

-- Opening (6477) Naga, Broken, Vrykul, Goblin
-- ALL 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 6477, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 6477, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 6477, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 6477, "Opening");

-- Opening (6478) Naga, Broken, Vrykul, Goblin
-- ALL 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 6478, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 6478, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 6478, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 6478, "Opening");

-- Attack (6603) Naga, Broken, Vrykul, Goblin
-- ALL 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 6603, "Attack");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 6603, "Attack");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 6603, "Attack");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 6603, "Attack");

-- Duel(7266) Naga, Broken, Vrykul, Goblin 
-- ALL
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 7266, "Duel");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 7266, "Duel");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 7266, "Duel");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 7266, "Duel");

-- Grovel (7267) Naga, Broken, Vrykul, Goblin
-- ALL
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 7267, "Grovel");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 7267, "Grovel");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 7267, "Grovel");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 7267, "Grovel");

-- Stuck (7355) Naga, Broken, Vrykul, Goblin
-- ALL
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 7355, "Stuck");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 7355, "Stuck");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 7355, "Stuck");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 7355, "Stuck");

-- Attacking (8386) Naga, Broken, Vrykul, Goblin
-- ALL
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 8386, "Attacking");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 8386, "Attacking");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 8386, "Attacking");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 8386, "Attacking");

-- Mail (8737) Naga, Broken, Vrykul, Goblin
-- Warrior, Paladin, Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x23, 8386, "Mail");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x23, 8386, "Mail");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x23, 8386, "Mail");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x23, 8386, "Mail");

-- Leather (9077) Naga, Broken, Vrykul, Goblin
-- Warrior, Paladin, Hunter, Rogue, Death Knight, Shaman, Druid
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x46F, 9077, "Attacking");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x46F, 9077, "Attacking");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x46F, 9077, "Attacking");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x46F, 9077, "Attacking");

-- Cloth (9078) Naga, Broken, Vrykul, Goblin
-- Classes: Warrior, Paladin, Shaman
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x43, 9078, "Attacking");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x43, 9078, "Attacking");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x43, 9078, "Attacking");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x43, 9078, "Attacking");

-- Generic (9125) Naga, Broken, Vrykul, Goblin
-- ALL
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 9125, "Generic");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 9125, "Generic");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 9125, "Generic");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 9125, "Generic");

-- Shield (9116) Naga, Broken, Vrykul, Goblin
-- ALL
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 9116, "Shield");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 9116, "Shield");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 9116, "Shield");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 9116, "Shield");

-- Opening (21651) Naga, Broken, Vrykul, Goblin
-- ALL
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 21651, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 21651, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 21651, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 21651, "Opening");

-- Closing (21652) Naga, Broken, Vrykul, Goblin
-- ALL
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 21652, "Closing");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 21652, "Closing");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 21652, "Closing");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 21652, "Closing");

-- Remove Insignia (22027) Naga, Broken, Vrykul, Goblin
-- ALL
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 22027, "Remove Insignia");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 22027, "Remove Insignia");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 22027, "Remove Insignia");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 22027, "Remove Insignia");

-- Opening - No Text (22810) Naga, Broken, Vrykul, Goblin
-- ALL
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 22810, "Opening - No Text");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 22810, "Opening - No Text");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 22810, "Opening - No Text");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 22810, "Opening - No Text");

-- Victorious State (32215) Naga, Broken, Vrykul, Goblin
-- Warrior
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x1, 32215, "Victorious State");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x1, 32215, "Victorious State");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x1, 32215, "Victorious State");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x1, 32215, "Victorious State");

-- Summon Friend (45927) Naga, Broken, Vrykul, Goblin
-- ALL
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 45927, "Summon Friend");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 45927, "Summon Friend");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 45927, "Summon Friend");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 45927, "Summon Friend");

-- Opening (61437) Naga, Broken, Vrykul, Goblin
-- ALL
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5FF, 61437, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5FF, 61437, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5FF, 61437, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5FF, 61437, "Opening");

-- Two-Handed Maces (199) Naga, Broken, Vrykul, Goblin
-- Human, Dwarf, Blood elf, Dranei
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x3, 199, "Two-Handed Maces");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x3, 199, "Two-Handed Maces");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x3, 199, "Two-Handed Maces");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x3, 199, "Two-Handed Maces");

-- Holy Light (635) Naga, Broken, Vrykul, Goblin
-- Paladin
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x2, 635, "Holy Light");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x2, 635, "Holy Light");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x2, 635, "Holy Light");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x2, 635, "Holy Light");

-- Seal of Righteousness (21084) Naga, Broken, Vrykul, Goblin
-- Paladin
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x2, 21084, "Seal of Righteousness");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x2, 21084, "Seal of Righteousness");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x2, 21084, "Seal of Righteousness");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x2, 21084, "Seal of Righteousness");

-- Libram (27762) Naga, Broken, Vrykul, Goblin
-- Paladin
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x2, 27762, "Libram");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x2, 27762, "Libram");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x2, 27762, "Libram");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x2, 27762, "Libram");

-- Daggers (1180) Naga, Broken, Vrykul, Goblin
-- Warrior, Hunter, Rogue, Warlock, Druid
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x50D, 1180, "Daggers");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x50D, 1180, "Daggers");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x50D, 1180, "Daggers");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x50D, 1180, "Daggers");

-- Daggers (1180) Naga, Broken, Vrykul, Goblin
-- Warrior, Hunter, Rogue, Warlock, Druid
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x50D, 1180, "Daggers");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x50D, 1180, "Daggers");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x50D, 1180, "Daggers");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x50D, 1180, "Daggers");

-- Sinister Strike (1752) Naga, Broken, Vrykul, Goblin
-- Rogue
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x8, 1180, "Sinister Strike");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x8, 1180, "Sinister Strike");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x8, 1180, "Sinister Strike");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x8, 1180, "Sinister Strike");

-- Eviscerate (2098) Naga, Broken, Vrykul, Goblin
-- Rogue
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x8, 2098, "Eviscerate");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x8, 2098, "Eviscerate");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x8, 2098, "Eviscerate");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x8, 2098, "Eviscerate");

-- Thrown (2567) Naga, Broken, Vrykul, Goblin
-- Warrior, Rogue
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x9, 2567, "Thrown");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x9, 2567, "Thrown");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x9, 2567, "Thrown");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x9, 2567, "Thrown");

-- Thrown (2764) Naga, Broken, Vrykul, Goblin
-- Warrior, Rogue
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x9, 2764, "Thrown");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x9, 2764, "Thrown");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x9, 2764, "Thrown");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x9, 2764, "Thrown");

-- Thrown (2764) Naga, Broken, Vrykul, Goblin
-- Warrior, Rogue
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x9, 2764, "Thrown");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x9, 2764, "Thrown");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x9, 2764, "Thrown");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x9, 2764, "Thrown");

-- Defensive State (DND) (16092) Naga, Broken, Vrykul, Goblin
-- Rogue
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x8, 16092, "Defensive State (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x8, 16092, "Defensive State (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x8, 16092, "Defensive State (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x8, 16092, "Defensive State (DND)");

-- Rogue Passive (DND) (21184) Naga, Broken, Vrykul, Goblin
-- Rogue
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x8, 21184, "Rogue Passive (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x8, 21184, "Rogue Passive (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x8, 21184, "Rogue Passive (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x8, 21184, "Rogue Passive (DND)");

-- Smite (585) Naga, Broken, Vrykul, Goblin
-- Priest
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x10, 585, "Smite");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x10, 585, "Smite");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x10, 585, "Smite");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x10, 585, "Smite");

-- Lesser Heal (2050) Naga, Broken, Vrykul, Goblin
-- Priest
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x10, 2050, "Lesser Heal");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x10, 2050, "Lesser Heal");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x10, 2050, "Lesser Heal");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x10, 2050, "Lesser Heal");

-- Wands (5009) Naga, Broken, Vrykul, Goblin
-- Priest, Mage, Warlock
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x190, 5009, "Wands");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x190, 5009, "Wands");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x190, 5009, "Wands");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x190, 5009, "Wands");

-- Shoot (5019) Naga, Broken, Vrykul, Goblin
-- Priest, Mage, Warlock
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x190, 5019, "Shoot");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x190, 5019, "Shoot");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x190, 5019, "Shoot");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x190, 5019, "Shoot");

-- Two-Handed Axes (197) Naga, Broken, Vrykul, Goblin
-- Warrior, Hunter, Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x25, 197, "Two-Handed Axes");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x25, 197, "Two-Handed Axes");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x25, 197, "Two-Handed Axes");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x25, 197, "Two-Handed Axes");

-- Polearms (200) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 200, "Polearms");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 200, "Polearms");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 200, "Polearms");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 200, "Polearms");

-- Two-Handed Swords (202) Naga, Broken, Vrykul, Goblin
-- Warrior, Paladin, Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x23, 202, "Two-Handed Swords");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x23, 202, "Two-Handed Swords");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x23, 202, "Two-Handed Swords");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x23, 202, "Two-Handed Swords");

-- Dual Wield (674) Naga, Broken, Vrykul, Goblin
-- Rogue, Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x28, 674, "Dual Wield");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x28, 674, "Dual Wield");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x28, 674, "Dual Wield");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x28, 674, "Dual Wield");

-- Plate Mail (750) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 750, "Plate Mail");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 750, "Plate Mail");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 750, "Plate Mail");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 750, "Plate Mail");

-- Parry (3127) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 3127, "Parry");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 3127, "Parry");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 3127, "Parry");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 3127, "Parry");

-- Linen Bandage (3275) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 3275, "Linen Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 3275, "Linen Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 3275, "Linen Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 3275, "Linen Bandage");

-- Heavy Linen Bandage (3276) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 3276, "Heavy Linen Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 3276, "Heavy Linen Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 3276, "Heavy Linen Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 3276, "Heavy Linen Bandage");

-- Wool Bandage (3277) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 3277, "Wool Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 3277, "Wool Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 3277, "Wool Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 3277, "Wool Bandage");

-- Heavy Wool Bandage (3278) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 3278, "Heavy Wool Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 3278, "Heavy Wool Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 3278, "Heavy Wool Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 3278, "Heavy Wool Bandage");

-- Silk Bandage (7928) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 7928, "Silk Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 7928, "Silk Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 7928, "Silk Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 7928, "Silk Bandage");


-- Heavy Silk Bandage (7934) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 7934, "Heavy Silk Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 7934, "Heavy Silk Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 7934, "Heavy Silk Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 7934, "Heavy Silk Bandage");

-- Anti-Venom (3127) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 3127, "Anti-Venom");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 3127, "Anti-Venom");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 3127, "Anti-Venom");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 3127, "Anti-Venom");

-- Mageweave Bandage (10840) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 10840, "Mageweave Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 10840, "Mageweave Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 10840, "Mageweave Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 10840, "Mageweave Bandage");

-- Heavy Mageweave Bandage (10841) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 10841, "Heavy Mageweave Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 10841, "Heavy Mageweave Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 10841, "Heavy Mageweave Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 10841, "Heavy Mageweave Bandage");

-- First Aid (10846) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 10846, "First Aid");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 10846, "First Aid");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 10846, "First Aid");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 10846, "First Aid");

-- Parry (3127) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 3127, "Parry");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 3127, "Parry");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 3127, "Parry");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 3127, "Parry");

-- Runecloth Bandage (18629) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 18629, "Runecloth Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 18629, "Runecloth Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 18629, "Runecloth Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 18629, "Runecloth Bandage");

-- Heavy Runecloth Bandage (18630) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 18630, "Heavy Runecloth Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 18630, "Heavy Runecloth Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 18630, "Heavy Runecloth Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 18630, "Heavy Runecloth Bandage");

-- Journeyman Riding (33391) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 33391, "Journeyman Riding");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 33391, "Journeyman Riding");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 33391, "Journeyman Riding");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 33391, "Journeyman Riding");

-- Plague Strike (45462) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 45462, "Plague Strike");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 45462, "Plague Strike");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 45462, "Plague Strike");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 45462, "Plague Strike");

-- Icy Touch (45477) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 45477, "Icy Touch");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 45477, "Icy Touch");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 45477, "Icy Touch");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 45477, "Icy Touch");

-- Blood Strike (45902) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 45902, "Blood Strike");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 45902, "Blood Strike");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 45902, "Blood Strike");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 45902, "Blood Strike");

-- Offensive State (DND) (45903) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 45903, "Offensive State (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 45903, "Offensive State (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 45903, "Offensive State (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 45903, "Offensive State (DND)");

-- Death Coil (47541) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 47541, "Death Coil");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 47541, "Death Coil");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 47541, "Death Coil");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 47541, "Death Coil");

-- Blood Presence (48266) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 48266, "Blood Presence");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 48266, "Blood Presence");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 48266, "Blood Presence");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 48266, "Blood Presence");

-- Death Grip (49576) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 49576, "Death Grip");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 49576, "Death Grip");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 49576, "Death Grip");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 49576, "Death Grip");

-- Forceful Deflection (49410) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 49410, "Forceful Deflection");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 49410, "Forceful Deflection");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 49410, "Forceful Deflection");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 49410, "Forceful Deflection");

-- Sigil (52665) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 52665, "Sigil");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 52665, "Sigil");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 52665, "Sigil");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 52665, "Sigil");

-- Blood Plague (59879) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 59879, "Parry");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 59879, "Parry");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 59879, "Parry");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 59879, "Parry");

-- Frost Fever (59921) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 59921, "Frost Fever");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 59921, "Frost Fever");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 59921, "Frost Fever");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 59921, "Frost Fever");

-- Runic Focus (61455) Naga, Broken, Vrykul, Goblin
-- Death Knight
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x20, 61455, "Runic Focus");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x20, 61455, "Runic Focus");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x20, 61455, "Runic Focus");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x20, 61455, "Runic Focus");

-- Fireball (133) Naga, Broken, Vrykul, Goblin
-- Mage
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x80, 133, "Fireball");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x80, 133, "Fireball");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x80, 133, "Fireball");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x80, 133, "Fireball");

-- Frost Armor (168) Naga, Broken, Vrykul, Goblin
-- Mage
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x80, 168, "Frost Armor");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x80, 168, "Frost Armor");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x80, 168, "Frost Armor");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x80, 168, "Frost Armor");

-- Staves (227) Naga, Broken, Vrykul, Goblin
-- Priest, Shaman, Mage, Warlock, Druid
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x5D0, 227, "Staves");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x5D0, 227, "Staves");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x5D0, 227, "Staves");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x5D0, 227, "Staves");

-- Shadow Bolt (686) Naga, Broken, Vrykul, Goblin
-- Warlock
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x100, 686, "Shadow Bolt");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x100, 686, "Shadow Bolt");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x100, 686, "Shadow Bolt");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x100, 686, "Shadow Bolt");

-- Demon Skin (687) Naga, Broken, Vrykul, Goblin
-- Warlock
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x100, 687, "Demon Skin");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x100, 687, "Demon Skin");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x100, 687, "Demon Skin");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x100, 687, "Demon Skin");

-- Chaos Bolt Passive (58284) Naga, Broken, Vrykul, Goblin
-- Warlock
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x100, 58284, "Chaos Bolt Passive");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x100, 58284, "Chaos Bolt Passive");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x100, 58284, "Chaos Bolt Passive");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x100, 58284, "Chaos Bolt Passive");

-- Auto Shot (75) Naga, Broken, Vrykul, Goblin
-- Hunter
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x4, 75, "Auto Shot");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x4, 75, "Auto Shot");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x4, 75, "Auto Shot");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x4, 75, "Auto Shot");

-- Bows (264) Naga, Broken, Vrykul, Goblin
-- Hunter
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x4, 264, "Bows");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x4, 264, "Bows");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x4, 264, "Bows");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x4, 264, "Bows");

-- Raptor Strike (2973) Naga, Broken, Vrykul, Goblin
-- Hunter
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x4, 2973, "Raptor Strike");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x4, 2973, "Raptor Strike");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x4, 2973, "Raptor Striket");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x4, 2973, "Raptor Strike");

-- Defensive State (DND) (13358) Naga, Broken, Vrykul, Goblin
-- Hunter
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x4, 13358, "Defensive State (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x4, 13358, "Defensive State (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x4, 13358, "Defensive State (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x4, 13358, "Defensive State (DND)");

-- Command (20576) Naga, Broken, Vrykul, Goblin
-- Hunter
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x4, 20576, "Command");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x4, 20576, "Command");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x4, 20576, "Command");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x4, 20576, "Command");

-- Defensive State 2 (DND) (24949) Naga, Broken, Vrykul, Goblin
-- Hunter
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x4, 24949, "Defensive State 2 (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x4, 24949, "Defensive State 2 (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x4, 24949, "Defensive State 2 (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x4, 24949, "Defensive State 2 (DND)");

-- Advantaged State (DND) (34082) Naga, Broken, Vrykul, Goblin
-- Hunter
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x4, 34082, "Advantaged State (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x4, 34082, "Advantaged State (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x4, 34082, "Advantaged State (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x4, 34082, "Advantaged State (DND)");

-- Healing Wave (331) Naga, Broken, Vrykul, Goblin
-- Shaman
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x40, 331, "Healing Wave");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x40, 331, "Healing Wave");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x40, 331, "Healing Wave");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x40, 331, "Healing Wave");

-- Lightning Bolt (403) Naga, Broken, Vrykul, Goblin
-- Shaman
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x40, 403, "Lightning Bolt");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x40, 403, "Lightning Bolt");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x40, 403, "Lightning Bolt");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x40, 403, "Lightning Bolt");

-- Totem (27763) Naga, Broken, Vrykul, Goblin
-- Orc, Tauren, Troll, Dranei
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x40, 27763, "Totem");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x40, 27763, "Totem");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x40, 27763, "Totem");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x40, 27763, "Totem");

-- Healing Wave (331) Naga, Broken, Vrykul, Goblin
-- Orc, Tauren, Troll, Dranei
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x40, 331, "Healing Wave");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x40, 331, "Healing Wave");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x40, 331, "Healing Wave");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x40, 331, "Healing Wave");

-- Guns (266) 
-- Hunter
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x4, 266, "Guns");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x4, 266, "Guns");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x4, 266, "Guns");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x4, 266, "Guns");

-- Bows (264) 
-- Hunter
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x4, 264, "Bows");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x4, 264, "Bows");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x4, 264, "Bows");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x4, 264, "Bows");

-- shoot (126354) 
-- Hunter
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x4, 126354, "Shoot");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 0x4, 126354, "Shoot");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 0x4, 126354, "Shoot");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 0x4, 126354, "Shoot");

REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x0, 0x4, 266, "Guns");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x0, 0x4, 264, "Bows");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x0, 0x4, 126354, "Shoot");




