REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (100800, 0, 0, 0, 0, 0, 24787, 0, 0, 0, 'Shadowsoul', 'Flayer of Gods', '', 0, 82, 82, 2, 16, 16, 0, 1, 1.5, 4, 3, 2000, 2480, 0, 300, 6, 2000, 0, 1, 0, 2048, 8, 0, 0, 0, 0, 0, 21, 30, 4, 4, 0, 0, 0, 0, 50, 50, 50, 50, 50, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5000000, 5000000, 'SmartAI', 0, 1, 1, 115, 10, 5, 0, 0, 0, 0, 0, 0, 0, 0, 1, 618348543, 0, '', 12340);

DELETE FROM `creature_text` WHERE `entry`=100800;
REPLACE INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100800,1,0,'Begin Fighting!!!',41,0,100,1,0,17459,'Comment');
REPLACE INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100800,2,0,'Phase 2!!!',41,0,100,1,0,17459,'Comment');
REPLACE INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100800,3,0,'Final Phase!!!',41,0,100,1,0,17459,'Comment');
REPLACE INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100800,4,0,'I will fill your sould with dark despair!',16,0,100,1,0,0,'Comment');
REPLACE INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100800,5,0,'My life for the Mordriak!!!',16,0,100,1,0,0,'Comment');
REPLACE INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100800,6,0,'You....will.....suffer and die!!!',16,0,100,1,0,0,'Comment');
REPLACE INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100800,7,0,'Tiny mortals, you cannot hurt me!!!',16,0,100,1,0,0,'Comment');
REPLACE INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100800,8,0,'What is this!!! What is happening!!!',16,0,100,1,0,0,'Comment');

-- Change Entry ID
SET @ENTRY := 100800;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
REPLACE INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,2,0,100,1,95,100,0,0,11,2565,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Shield Block"),
(@ENTRY,@SOURCETYPE,1,0,2,0,100,0,90,95,15000,19000,11,38819,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Death_Blast"),
(@ENTRY,@SOURCETYPE,2,0,2,0,100,0,85,90,15000,19000,11,72133,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Pain and Suffering"),
(@ENTRY,@SOURCETYPE,3,0,2,0,100,0,80,85,4000,6000,11,32950,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Eye Beam"),
(@ENTRY,@SOURCETYPE,4,0,2,0,100,1,75,80,0,0,11,69409,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Soul Reaper"),
(@ENTRY,@SOURCETYPE,5,0,2,0,100,0,70,75,15000,19000,11,72264,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Delirious Slash"),
(@ENTRY,@SOURCETYPE,6,0,2,0,100,0,65,70,13000,15000,11,73070,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Incite Terror"),
(@ENTRY,@SOURCETYPE,7,0,2,0,100,0,60,65,10000,13000,11,31984,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Finger_of_death"),
(@ENTRY,@SOURCETYPE,8,0,2,0,100,0,55,60,15000,20000,11,71289,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Dominate Mind"),
(@ENTRY,@SOURCETYPE,9,0,2,0,100,0,45,50,10000,13000,11,71254,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Shadow Bolt"),
(@ENTRY,@SOURCETYPE,10,0,2,0,100,0,40,45,13000,15000,11,59971,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Rain of Fire"),
(@ENTRY,@SOURCETYPE,11,0,2,0,100,0,35,40,13000,15000,11,68141,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Death Coil"),
(@ENTRY,@SOURCETYPE,12,0,2,0,100,0,30,35,15000,20000,11,40585,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Dark Barrage"),
(@ENTRY,@SOURCETYPE,13,0,2,0,100,0,25,30,5000,7000,11,39329,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Void Bolt"),
(@ENTRY,@SOURCETYPE,14,0,2,0,100,0,15,20,5000,7000,11,72170,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Flamestrike"),
(@ENTRY,@SOURCETYPE,15,0,2,0,100,0,10,15,5000,8000,11,30383,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Hateful Bolt"),
(@ENTRY,@SOURCETYPE,16,0,2,0,100,1,0,5,0,0,11,47008,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Berserk"),
(@ENTRY,@SOURCETYPE,17,0,2,0,100,1,60,100,0,0,1,1,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Phase 1"),
(@ENTRY,@SOURCETYPE,18,0,2,0,100,1,30,60,0,0,1,2,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Phase 2"),
(@ENTRY,@SOURCETYPE,19,0,2,0,100,1,1,30,0,0,1,3,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Phase 3"),
(@ENTRY,@SOURCETYPE,20,0,2,0,100,1,75,95,0,0,1,4,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"YELL4"),
(@ENTRY,@SOURCETYPE,21,0,2,0,100,1,55,75,0,0,1,5,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 5"),
(@ENTRY,@SOURCETYPE,22,0,2,0,100,1,35,55,0,0,1,6,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 6"),
(@ENTRY,@SOURCETYPE,23,0,2,0,100,1,15,35,0,0,1,7,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 7"),
(@ENTRY,@SOURCETYPE,24,0,2,0,100,1,1,15,0,0,1,8,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 8");