-- Siferos Equipment
delete from `creature_equip_template` where entry = 100100;
insert into `creature_equip_template` (`entry`, `id`, `itemEntry1`, `itemEntry2`, `itemEntry3`) values('100100','1','18842','0','0');

-- Cragnor Equipment
delete from `creature_equip_template` where entry = 100400;
insert into `creature_equip_template` (`entry`, `id`, `itemEntry1`, `itemEntry2`, `itemEntry3`) values('100400','1','18002','0','0');

-- Frizolial Equipment
delete from `creature_equip_template` where entry = 100300;
insert into `creature_equip_template` (`entry`, `id`, `itemEntry1`, `itemEntry2`, `itemEntry3`) values('100300','1','25622','0','0');

-- Ignial Equipment
delete from `creature_equip_template` where entry = 100200;
insert into `creature_equip_template` (`entry`, `id`, `itemEntry1`, `itemEntry2`, `itemEntry3`) values('100200','1','18842','0','0');

-- Mordrac Equipment
delete from `creature_equip_template` where entry = 100600;
insert into `creature_equip_template` (`entry`, `id`, `itemEntry1`, `itemEntry2`, `itemEntry3`) values('100600','1','18203','18202','0');

-- Vadri Equipment
delete from `creature_equip_template` where entry = 100500;
insert into `creature_equip_template` (`entry`, `id`, `itemEntry1`, `itemEntry2`, `itemEntry3`) values('100500','1','18002','0','0');

-- Zalistra Equipment
delete from `creature_equip_template` where entry = 100700;
insert into `creature_equip_template` (`entry`, `id`, `itemEntry1`, `itemEntry2`, `itemEntry3`) values('100700','1','31289','0','0');

-- ShadowSoul Equipment
delete from `creature_equip_template` where entry = 100800;
insert into `creature_equip_template` (`entry`, `id`, `itemEntry1`, `itemEntry2`, `itemEntry3`) values('100800','1','18002','0','0');

-- Plague Spitter Equipment
delete from `creature_equip_template` where entry = 200100;
insert into `creature_equip_template` (`entry`, `id`, `itemEntry1`, `itemEntry2`, `itemEntry3`) values('200100','1','31289','0','0');

-- Spine Breaker Equipment
delete from `creature_equip_template` where entry = 200200;
insert into `creature_equip_template` (`entry`, `id`, `itemEntry1`, `itemEntry2`, `itemEntry3`) values('200200','1','18002','0','0');














