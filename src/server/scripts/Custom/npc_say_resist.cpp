#include "ScriptPCH.h"

class npc_resist : public CreatureScript
{
	public : 
		npc_resist() : CreatureScript("npc_resist") {}

	struct npc_resistAI : public ScriptedAI
	{
		npc_resistAI(Creature* c) : ScriptedAI(c) {}
		
		uint32 speak_timer;

		void UpdateAI(uint32 diff)
		{
			if (speak_timer <= diff)
			{
				me->MonsterSay("...Do not Resist! It is futile!...", LANG_UNIVERSAL, 0); // Change this to change what NPC says
				speak_timer = 60000; // Changed to 1 Min
			} else speak_timer -= diff;
		}
		
		void Reset()
		{
			speak_timer = 60000; // Changed to 1 Min 
		}
	};
	
	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_resistAI(creature);
	}
};

void AddSC_npc_resist()
{
	new npc_resist();
}