/***************************************\
#  Copyright (C) Umbra Gaminc Inc 2013  #
*         By Nobody @ UmbraCore4        *
#            UC4 -> 255 Core            # 
\***************************************/

/* ScriptData
SDCategory: Karazhan
SDName: Boss_Penumbris
SD%Complete: 75
SDComment: Done until after PTR
EndScriptData */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

enum Penumbris
{
        
	////////////////////////
    //Penumbris Spell Info//
	////////////////////////
    
	//////////////
	//Boss Buffs//
	//////////////
	SPELL_SHADOW_DAMAGE_BUFF              = 18025, // Boss Buffs Itself-> Increases shadow spell power by 50.
	SPELL_DARK_MEND                       = 15068, // Instant Cast-> Heal Self for 50001
	SPELL_POWER_CIRCLE                    = 45042, // Conjures a Power Circle lasting for 15 sec. While standing in this circle, the caster gains 320 spell power.
	SPELL_REGEN                           = 55694, // Instantly heals you for 10% of your total health, and an additional 10% over 5 sec. 
	SPELL_EMPOWERED_BLOOD                 = 70227, // Damage done increased by 100%. Spells have no cost and are instant. Melee and ranged attack speed increased 100%.  
	/////////////////
	//DAMAGE SPELLS//
	/////////////////
	SPELL_CIRCLE_OF_DESTRUCTION           = 72323, // A circle of destruction is unleashed, causing 3194 - 3806 Shadow damage and knockback to enemies within 10 yds of the target.
	SPELL_DARK_GLARE                      = 26029, // Deals 43750 - 56250 Shadow damage to all enemies in front of the caster.  
	SPELL_DARK_MATTER                     = 51012, // A cloud of Dark Matter surrounds the target, increasing damage taken by 50%, decreasing movement speed by 30% and dealing 1885 - 2115 Shadow damage to nearby enemies.
	SPELL_DARK_SLASH                      = 48292, // Slashes the target with darkness, dealing damage equal to half the target's current health.
	SPELL_BERSERK                         = 41924, // Soft Enrage
	SPELL_HOD                             = 70063, // Wipe Entire Raid - Spell -> Fury of Frostmourn
    SPELL_INCREASE                        = 47816, // Increase spell power by 183 for 20 seconds
	
};

// Defined Yells with Proper Blizzlike sound to make boss more Professional -> Tested in game -> Working
enum Yells
{
    SAY_AGGRO                                     = 0, // 0 == Group ID for Creature_Text Table -> Sound included.
	SAY_PATHETIC                                  = 1,
	SAY_NOT_OVER                                  = 2,
	SAY_IMPOSSIBLE                                = 3, // use @ 10% Health
};

//Emotes_Buff
#define EMOTE_MEND         "%s drawns on dark energy to renew himself!" 
#define EMOTE_BUFF         "%s consumed dark energy and strenthens himself" 
#define EMOTE_POWER        "%s places a circle of power at his feet!" 
//Emote_Battle
#define EMOTE_GLARE        "%s casts a dark glare at a random hero!!! "
#define EMOTE_CIRCLE       "%s sets a dark circle of magic around a random hero!!! Quickly move away from them!!!" 
#define EMOTE_MATTER       "%s sets a cloud of dark matter around a random hero!!! Quickly move away from them!!!"
#define Emote_SLASH        "%s deals dark slashing blow!"

//Emote_Other
#define EMOTE_SLAY         "%s Slowly Dies!!!" 
#define EMOTE_HOD          "You feel the hand of death tearing at your soul as you die!"
#define EMOTE_INCREASE     "%s pulls more shadow power from the darkness"


class boss_penumbris : public CreatureScript
{
public:
    boss_penumbris() : CreatureScript("boss_penumbris") { }
	
	CreatureAI* GetAI(Creature* creature) const
    {
        return new boss_penumbrisAI (creature);
    }

	struct boss_penumbrisAI : public ScriptedAI
    {
        boss_penumbrisAI(Creature* creature) : ScriptedAI(creature) {}

        uint32 Regen_Timer;
		uint32 Enrage_Timer;
		uint32 Shadowbuff_Timer;
        uint32 Darkmend_Timer;
		uint32 Powercircle_Timer;
        uint32 Circle_Timer;
		uint32 Darkglare_Timer;
		uint32 Darkmatter_Timer;
		uint32 Darkslash_Timer;
		uint32 Hod_Timer;
		uint32 Increase_Timer;
		bool Enraged;	
		
		void Reset()
        {
            Regen_Timer           = 35000;
			Powercircle_Timer     = 15000;
			Circle_Timer          = 15000;
		    Darkmatter_Timer      = 20000;
            Shadowbuff_Timer      = 25000; 
			Increase_Timer        = 30000; // Gain 183 spell power for 20 seconds 
			Darkglare_Timer       = 45000; // Dark Glare Every 45 seconds 
			Darkmend_Timer        = 100000;
            Enrage_Timer          = 300000; 
            Hod_Timer             = 650000;
			Enraged = false;
			
        }
		
		void KilledUnit(Unit* /*Victim*/)
        {
            Talk(SAY_PATHETIC);
		}
		void JustDied(Unit* /*killer*/)
        {
            Talk(SAY_NOT_OVER);
			me->MonsterTextEmote(EMOTE_SLAY, 0, true);
        }
		void EnterCombat(Unit* /*who*/)
        {
            Talk(SAY_AGGRO);
        }
		void UpdateAI(uint32 diff)
        {
            if (!UpdateVictim())
                return;
            
			if (Enrage_Timer <=diff && !Enraged)
            {
                DoCast(me, SPELL_BERSERK);
				DoCast(me, SPELL_EMPOWERED_BLOOD);
				Enraged = true;
            } 
			else 
			    Enrage_Timer -= diff;

            if (Darkmend_Timer <= diff)
            {
                DoCast(me, SPELL_DARK_MEND);   
                Darkmend_Timer = 100000;
				me->MonsterYell("The Shadows Restore Me!!!", LANG_UNIVERSAL, me->GetGUID());
				me->MonsterTextEmote(EMOTE_MEND, 0, true);
            } else Darkmend_Timer -= diff;
			
			if (Powercircle_Timer <= diff)
            {
                DoCast(me, SPELL_POWER_CIRCLE);   
                me->MonsterTextEmote(EMOTE_POWER, 0, true);
				Powercircle_Timer = 15000;
			} else Powercircle_Timer -= diff;
			
			if (Regen_Timer <= diff)
            {
                DoCast(me, Regen_Timer, true);  
                me->MonsterTextEmote(EMOTE_MEND, 0, true);
				me->MonsterYell("The Shadows Restore Me!!!", LANG_UNIVERSAL, me->GetGUID());
				Regen_Timer = 35000;
			} else Regen_Timer -= diff;

            if (Shadowbuff_Timer <= diff)
            {
                DoCast(me, SPELL_SHADOW_DAMAGE_BUFF);
                me->MonsterTextEmote(EMOTE_BUFF, 0, true);
				me->MonsterYell("My Power Grows!!!", LANG_UNIVERSAL, me->GetGUID());
                Shadowbuff_Timer = 25000;       
            } else Shadowbuff_Timer -= diff;
            
			if (Circle_Timer <= diff)
            {
                if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
				    DoCast(Target, SPELL_CIRCLE_OF_DESTRUCTION);
					me->MonsterTextEmote(EMOTE_CIRCLE, 0, true);
                    me->MonsterYell("Stand together and die together!!!", LANG_UNIVERSAL, me->GetGUID());
				    Circle_Timer = 15000;       
            } else Circle_Timer -= diff;
            
			if (Darkmatter_Timer <= diff)
            {
                if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
					DoCast(Target, SPELL_DARK_MATTER);
                    me->MonsterTextEmote(EMOTE_MATTER, 0, true);
					me->MonsterYell("Feel my darkness surround you!!!", LANG_UNIVERSAL, me->GetGUID());
					Darkmatter_Timer = 20000;      
            } else Darkmatter_Timer -= diff;

            if (Darkglare_Timer <= diff)
            {
                if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
					DoCast(Target, SPELL_DARK_GLARE);
                    me->MonsterTextEmote(EMOTE_GLARE, 0, true);
				    Darkglare_Timer = 45000;        
			} else Darkglare_Timer -= diff;
				
			if (Hod_Timer <= diff)
            {
                DoCast(me->getVictim(), SPELL_HOD);
                me->MonsterTextEmote(EMOTE_HOD, 0, true);
				me->MonsterYell("The shadows will take you now, and all will perish in the deep darkness.", LANG_UNIVERSAL, me->GetGUID());
                Hod_Timer = 650000;         
            } else Hod_Timer -= diff;
			    
			if (Increase_Timer <= diff)
            {
                DoCast(me, SPELL_INCREASE);
                me->MonsterTextEmote(EMOTE_INCREASE, 0, true);
				me->MonsterYell("The sweet tase of darkness empowers me!!!", LANG_UNIVERSAL, me->GetGUID());
                Increase_Timer = 30000;         
            } else Increase_Timer -= diff;
				
            DoMeleeAttackIfReady();
        }
    };
};

void AddSC_boss_penumbris()
{
    new boss_penumbris();
}
