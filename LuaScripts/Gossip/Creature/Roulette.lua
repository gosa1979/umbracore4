local Npc_Id = 90000

function Roulette_Gossip(event, plr, unit)
    plr:GossipMenuAddItem(0, "I'm Ready to play the game of luck", 0, 1)
	plr:GossipMenuAddItem(0, "Nevermind", 0, 2)
    plr:GossipSendMenu(1, unit)
end
 
function Roulette_Event(event, plr, unit, sender, intid, code)
        if(intid == 1) then
                local rand = math.random(1,4)
                if (rand == 1)  then
                        plr:Kill()
                        unit:SendUnitSay("Hahahaha!", 0)
                        plr:PlayDirectSound(11965, plr) --Headless Horseman Laugh
                elseif (rand == 2) then
                        unit:SendUnitSay("You've won.. for now.", 0)
                        plr:PlayDirectSound(7914, plr) --Sigh
                elseif (rand == 3) then
                        plr:CastSpell(13566) --knockback 500 triggers
                        unit:SendUnitYell("YOU ARE NOT PREPARED!!", 0)
                        plr:PlayDirectSound(11466, plr) --Illidan "You are not prepared!"                      
                elseif (rand == 4) then
                        unit:SendUnitSay("Lets rock!", 0)
                        plr:PlaySoundToSet(11803, plr) --L80ETC Song
                end
        end
		
	if(intid == 2) then
		plr:GossipComplete()
	end		
end
 
 
RegisterCreatureGossipEvent(Npc_Id, 1, Roulette_Gossip)
RegisterCreatureGossipEvent(Npc_Id, 2, Roulette_Event)